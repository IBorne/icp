#!/bin/sh

builddir=build

rm -rf $builddir

cmake -B $builddir && make -C $builddir
