#include "icp.hpp"

cloud read_csv(std::string filename)
{
    char delim = ',';
    std::string line, left, right;
    cloud cloud;
    std::ifstream input(filename);
    if (!input.is_open())
        throw std::runtime_error("Could not open file");
    while (std::getline(input, line))
    {
        std::stringstream ss(line);
        std::size_t idx = line.find(delim);
        left = line.substr(0, idx);
        right = line.substr(idx + 1, line.length());
        cloud.emplace_back(std::stof(left), std::stof(right));
    }
    input.close();
    return cloud;
}

void print_cloud(cloud cloud)
{
    std::cout << "cloud: list of " << cloud.size() << " points" << std::endl;
    for (auto &p : cloud)
        std::cout << "  [" << p.first << "," << p.second << "]" << std::endl;
}
