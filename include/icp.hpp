#pragma once

#include <Eigen/Dense>
#include <cmath>
#include <err.h>
#include <fstream>
#include <iostream>
#include <iterator>
#include <limits>
#include <math.h>
#include <pcl/console/time.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <sstream>
#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <utility>
#include <vector>

#define MAX_ITER 10 // to determine
#define ICP_TRESHOLD 0.00001f
#define TRESHOLD 10 // to determine

typedef std::vector<std::pair<float, float>> cloud;

typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloudT;

typedef Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> matrix;

// display.cpp

typedef void (*icp_iteration)(PointCloudT::Ptr dst, PointCloudT::Ptr src);
void display_clouds(PointCloudT::Ptr dst, PointCloudT::Ptr src,
                    PointCloudT::Ptr curr, icp_iteration do_icp);

// parsing.cpp

PointCloudT::Ptr parse_cloud(std::string filename);

// utils.cpp

#define POWER_ITERATIONS 10

float mid_xyz(float a, float b);
void icp(PointCloudT::Ptr a, PointCloudT::Ptr b);
// end of utils.cpp

cloud read_csv(std::string filename);
void print_cloud(cloud cloud);
